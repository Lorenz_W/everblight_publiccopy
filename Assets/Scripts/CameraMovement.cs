﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public float panSpeed = 20f;
    public float panBorderThickness = 10f;

    [SerializeField]
    public Vector2 panLimit;

    public float scrollSpeed = 20f;
    public float minY = 20f;
    public float maxY = 20f;

	[SerializeField] float rotationSpeed = 200f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!PlayerMoveController.IsInputLocked)
		{
			Vector3 pos = transform.position;

			if (Input.GetKey("w") || (Input.mousePosition.y >= Screen.height - panBorderThickness && !Input.GetMouseButton(1)))
			{
				pos += panSpeed * new Vector3(transform.forward.x, 0f, transform.forward.z) * Time.deltaTime;
			}

			if (Input.GetKey("s") || (Input.mousePosition.y <= panBorderThickness && !Input.GetMouseButton(1)))
			{
				pos -= panSpeed * new Vector3(transform.forward.x, 0f, transform.forward.z) * Time.deltaTime;
			}

			if (Input.GetKey("d") || (Input.mousePosition.x >= Screen.width - panBorderThickness && !Input.GetMouseButton(1)))
			{
				pos += panSpeed * new Vector3(transform.right.x, 0f, transform.right.z) * Time.deltaTime;
			}

			if (Input.GetKey("a") || (Input.mousePosition.x <= panBorderThickness && !Input.GetMouseButton(1)))
			{
				pos -= panSpeed * new Vector3(transform.right.x, 0f, transform.right.z) * Time.deltaTime;
			}

			float scroll = Input.GetAxis("Mouse ScrollWheel");
			pos.y -= scroll * scrollSpeed * 100f * Time.deltaTime;
			pos.z += scroll * scrollSpeed * 100f * Time.deltaTime;

			pos.x = Mathf.Clamp(pos.x, -panLimit.x, panLimit.x);
			pos.y = Mathf.Clamp(pos.y, minY, maxY);
			pos.z = Mathf.Clamp(pos.z, -panLimit.y, panLimit.y);

			transform.position = pos;
		}

		if(Input.GetMouseButton(1))
		{
			float rotationInputX = Input.GetAxis("Mouse X");
			float rotationInputY = Input.GetAxis("Mouse Y");
			Quaternion newRotation = Quaternion.Euler(
				transform.rotation.eulerAngles.x + 0.0f,
				transform.rotation.eulerAngles.y + rotationInputX,
				transform.rotation.eulerAngles.z +0.0f);
			transform.rotation = Quaternion.RotateTowards(transform.rotation, newRotation, rotationSpeed);
		}
	}
}
