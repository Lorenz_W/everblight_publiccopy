﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeIcon : MonoBehaviour {

    [SerializeField]
    GameObject biologist, soldier, scientist;

    [SerializeField]
    Text characterName, characterClass;

    [SerializeField]
    string biologistName, soldierName, scientistName;
    

    public void changeIcon(string name) {

        switch (name)
        {
            case "character_biologist":
                setActiveBiologist();
                break;
            case "character_scientist":
                setActiveScientist();
                break;
            case "character_soldier":
                GetComponent<ChangeIcon>().setActiveSoldier();
                break;
            default:
                Debug.Log("Character name changed to:" + name);
                break;
        }

    }

    public void setActiveBiologist() {
        biologist.SetActive(true);
        soldier.SetActive(false);
        scientist.SetActive(false);

        characterClass.text = "BIOLOGIST";
        characterName.text = biologistName;
    }

    public void setActiveSoldier()
    {
        biologist.SetActive(false);
        soldier.SetActive(true);
        scientist.SetActive(false);

        characterClass.text = "SOLDIER";
        characterName.text = soldierName;
    }
    public void setActiveScientist()
    {
        biologist.SetActive(false);
        soldier.SetActive(false);
        scientist.SetActive(true);

        characterClass.text = "SCIENTIST";
        characterName.text = scientistName;
    }
}
