﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMoveController : MonoBehaviour
{
	[SerializeField] public int AIMoveRange;
	[SerializeField] public EnemyCharacter[] AICharacters;
	

	public static AIMoveController instance = null;
	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		} else
		{
			Debug.LogWarning("More than one instance of AIMoveController in scene!");
		}
	}

	public void MoveAllAIToNearestPlayer()
	{
		GridSystem.CheckForBlockedNodes(false);
		StartCoroutine(ScheduleAILerpMove());
	}

	public List<GridNode> FindPathToNearestPlayer(GameObject startPos)
	{
		List<GridNode> pathToPlayer = new List<GridNode>();
		GridNode startNode = GridSystem.GetNodeFromWorldPosition(startPos.transform.position);
		//Creates a value higher than any non buggy distance can get.
		int minDistance = GridSystem.instance.gridSize.x + GridSystem.instance.gridSize.y + 2;


		for (int i = 0; i < Bait.baitLocactions.Count; ++i)
		{
			int distanceToBait = PathFinding.instance.GetDistance(startNode, Bait.baitLocactions[i]);
			if (Bait.placedBaitEffectRange >= distanceToBait)
			{
				List<GridNode> pathToBait = PathFinding.instance.FindPath(startNode.worldPosition, Bait.baitLocactions[i].worldPosition, false);
				if(pathToBait.Count < minDistance)
				{
					minDistance = pathToBait.Count;
					pathToPlayer = pathToBait;
				}
			}
		}
		//If a bait was in move range, return early.
		if(minDistance < GridSystem.instance.gridSize.x + GridSystem.instance.gridSize.y + 2)
		{
			return pathToPlayer;
		}

		//Every empty tile adjacent to a player characters tile.
		List<GridNode> positionsToArriveAt = new List<GridNode>();
		for (int i = 0; i < PlayerMoveController.instance.playerCharacters.Count; ++i)
		{
			if (PlayerMoveController.instance.playerCharacters[i].roundsInvisible > 0)
			{
				continue;
			}
			positionsToArriveAt.Add(
				GridSystem.GetNodeFromWorldPosition(PlayerMoveController.instance.playerCharacters[i].transform.position));
		}

		int indexOfNearestNode = -1;
		

		for (int i = 0; i < positionsToArriveAt.Count; ++i)
		{
			if (positionsToArriveAt[i].isBlocked && !GridSystem.IsNodeOcupiedByPlayerChar(positionsToArriveAt[i]))
			{
				continue;
			}
			int nextDistance = PathFinding.instance.GetDistance(startNode, positionsToArriveAt[i]);
			if (minDistance > nextDistance)
			{
				minDistance = nextDistance;
				indexOfNearestNode = i;
			}
		}

		if (positionsToArriveAt.Count < indexOfNearestNode || indexOfNearestNode < 0)
		{
			return null;
		}
		pathToPlayer = PathFinding.instance.FindPath(
			startPos.transform.position, 
			positionsToArriveAt[indexOfNearestNode].worldPosition,
			false);
		return pathToPlayer;
	}

	private List<GridNode> CheckForTrapAndBaitInPath(EnemyCharacter enemyToCheckFor, List<GridNode> pathToCheck)
	{
		for (int j = 0; j < AIMoveRange && j < pathToCheck.Count; ++j)
		{
			if (pathToCheck[j].hasTrap || pathToCheck[j].hasBait)
			{
				if (pathToCheck[j].hasTrap)
				{
					Debug.Log("AI moved less due to stepping in trap.");
					enemyToCheckFor.roundsImobilized = pathToCheck[j].trapImobilizeDuration;
					Debug.Log(enemyToCheckFor.gameObject.name + " is now trapped for " + enemyToCheckFor.roundsImobilized + " rounds.");

					pathToCheck[j].hasTrap = false;
					GridSystem.blockedSpaceObjects.Remove(pathToCheck[j].trapObject);
					Destroy(pathToCheck[j].trapObject);
					if (GridSystem.IsNodeOcupiedByPlayerChar(pathToCheck[j]))
					{
						TurnController.SetTurnTo(TurnController.TurnOrder.Lose);
					}
				}
				if (pathToCheck[j].hasBait)
				{
					Debug.Log("AI moved less due to discovering a bait.");

					pathToCheck[j].hasBait = false;
					GridSystem.blockedSpaceObjects.Remove(pathToCheck[j].baitObject);
					Destroy(pathToCheck[j].baitObject);
					if (GridSystem.IsNodeOcupiedByPlayerChar(pathToCheck[j]))
					{
						TurnController.SetTurnTo(TurnController.TurnOrder.Lose);
					}
				}

				pathToCheck.RemoveRange(j, pathToCheck.Count - j);
				return pathToCheck;
			}
		}
		return pathToCheck;
	}

	private IEnumerator ScheduleAILerpMove()
	{
		List<GridNode> currentAIPath = new List<GridNode>();
		for (int i = 0; i < AICharacters.Length; ++i)
		{
			if (AICharacters[i].roundsImobilized > 0)
			{
				--AICharacters[i].roundsImobilized;
				continue;
			}
			currentAIPath = FindPathToNearestPlayer(AICharacters[i].gameObject);
			if (currentAIPath == null)
			{
				continue;
			}

			currentAIPath = CheckForTrapAndBaitInPath(AICharacters[i], currentAIPath);
			if (currentAIPath.Count > AIMoveRange)
			{
				yield return StartCoroutine(PlayerMoveController.instance.LerpMovement(
					AICharacters[i].gameObject,
					AIMoveRange,
					currentAIPath));
			}
			else
			{
				yield return StartCoroutine(PlayerMoveController.instance.LerpMovement(
					AICharacters[i].gameObject,
					currentAIPath.Count,
					currentAIPath));
			}
		}

		GridSystem.UpdateGrid(true);
		yield return null;
	}
}
