﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CutsceneText : MonoBehaviour
{
	[SerializeField] private Canvas canvas;
	[SerializeField] private Text textField;
	[SerializeField] private string[] texts;
	[SerializeField] private AudioClip[] sound;
	private int currentTextIndex = -1;

	// Use this for initialization
	void Awake()
	{
		StopShowingText();
	}

	//// Update is called once per frame
	//void Update()
	//{
	//
	//}

	public void StartShowingText()
	{
		Debug.Log("Started showing cutscene text.");
		currentTextIndex = 0;
		ShowNextText();
	}
	public void StopShowingText()
	{
		canvas.enabled = false;
		Debug.Log("Stopped showing cutscene text.");
	}
	public void ResumeShowingText()
	{
		Debug.Log("Resumed showing cutscene text.");
		--currentTextIndex;
		ShowNextText();
	}

	public void ShowNextText()
	{
		if(currentTextIndex >= texts.Length)
		{
			textField.enabled = false;
			canvas.enabled = false;
			Debug.Log("End of cutscene texts reached.");
			return;
		}
		canvas.enabled = true;
		textField.enabled = true;

		textField.text = texts[currentTextIndex];

		++currentTextIndex;
	}
}
