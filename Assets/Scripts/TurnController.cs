﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TurnController {
	public enum TurnOrder {
		Player,
		AI,
		Pause,
		Win,
		Lose
	};

	static public int currentTurn = 0;

	private static TurnOrder turnOf = TurnOrder.Pause;
	public static TurnOrder GetTurnOf() {
		return turnOf;
	}
	public static bool SetTurnTo(TurnOrder turnToSet) {
		if(turnToSet == turnOf) {
			Debug.Log("It is already the turn of \"" + turnOf.ToString() + "\"");
			return false;
		}
		turnOf = turnToSet;

		if(turnOf == TurnOrder.AI) {
			Time.timeScale = 1f;
			SkillsMenu.instance.CloseMenu();
			PlayerMoveController.IsInputLocked = true;
			AIMoveController.instance.MoveAllAIToNearestPlayer();
			SetTurnTo(TurnOrder.Player);
            SystemMessageMenu.instance.OpenMenu("ENEMIES MOVED!");
        }
		else if(turnOf == TurnOrder.Player) {
			++currentTurn;
			Time.timeScale = 1f;
			PlayerMoveController.IsInputLocked = false;
			for (int i = 0; i < PlayerMoveController.instance.playerCharacters.Count; ++i) {
				PlayerMoveController.instance.playerCharacters[i].currentActionsLeft = PlayerMoveController.instance.playerCharacters[i].actionsPerTurn;
				if (PlayerMoveController.instance.playerCharacters[i].roundsInvisible > 0)
				{
					PlayerMoveController.instance.playerCharacters[i].roundsInvisible -= 1;
					//Remove visuals if we stop being invisible this turn.
					if(PlayerMoveController.instance.playerCharacters[i].roundsInvisible == 0)
					{
						foreach (ParticleSystem visualization in Invisible.instance.InvisibleVisualization)
						{
							visualization.Stop();
						}
					}
				} 
			}
			SkillsMenu.instance.OpenMenu();
		} else if(turnOf == TurnOrder.Pause) {
			Time.timeScale = 0f;
			SkillsMenu.instance.CloseMenu();
		} else if(turnOf == TurnOrder.Lose) {
			for(int i = 0; i < PlayerMoveController.instance.playerCharacters.Count; ++i) {
				if (GridSystem.IsNodeOcupiedByAI(
					GridSystem.GetNodeFromWorldPosition(
						PlayerMoveController.instance.currentPlayer.gameObject.transform.position))) {
					SystemMessageMenu.instance.OpenMenu("The character " + PlayerMoveController.instance.playerCharacters[i].gameObject.name 
						+ " was catched by the bacteria. You lost!\n"
						+ "Restart the level through the level selection menu.");
					GameObject.Destroy(PlayerMoveController.instance.playerCharacters[i].gameObject);
				}
			}
			
		} else if(turnOf == TurnOrder.Win) {
			CutsceneTextController.instance.ActivateNextCutsceneText();
			SystemMessageMenu.instance.OpenMenu("You reached the target area. You Win!\n" +
				"Select a new level from the level selection menu.");
		}
		return true;
	}
}