﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectionMenu : Menu {
	public void LoadDifferentScene(string scene) {
		Debug.Log("Loading scene: " + scene);
		SceneManager.LoadScene(scene);
	}
}
