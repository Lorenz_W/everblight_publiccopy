﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillsMenu : Menu {
	[SerializeField] Button[] skillButtons;
	[SerializeField] Button backButton;
	[SerializeField] Text hoverText;

	public static SkillsMenu instance = null;
	private void Awake() {
		if (instance == null) {
			instance = this;
		}
	}

	private void Start() {
	}

	override protected void Update() {
		if (!PlayerMoveController.IsInputLocked)
		{
			if (Input.GetKeyDown(KeyCode.Alpha1))
			{
				skillButtons[0].onClick.Invoke();
			}
			if (Input.GetKeyDown(KeyCode.Alpha2))
			{
				skillButtons[1].onClick.Invoke();
			}
			if (Input.GetKeyDown(KeyCode.Alpha3))
			{
				skillButtons[2].onClick.Invoke();
			}
			if (Input.GetKeyDown(KeyCode.Alpha4))
			{
				skillButtons[3].onClick.Invoke();
			}
		}
	}

	override public void OpenMenu() {
		GridSystem.visualizedSkillRange.Clear();
		Skills.currentSkill = Skills.AllSkills.None;
		Skills.IsTileMarkingActive = false;
		
		CloseMenu();
		base.OpenMenu();
		SetSkillbuttonsVisiblity(false);
		backButton.gameObject.SetActive(false);

		Skills[] currentPlayerSkills = PlayerMoveController.instance.currentPlayer.skills;
		for(int i = 0; i < currentPlayerSkills.Length; ++i) {
			skillButtons[i].gameObject.SetActive(true);
			Text skillText = skillButtons[i].GetComponentInChildren<Text>();
			skillText.text = 
				currentPlayerSkills[i].thisSkill + "\n"
				+ "Uses left: " + currentPlayerSkills[i].skillUsesLeft + "\n"
				+ "Range: " + currentPlayerSkills[i].skillRange;
			skillText.canvasRenderer.SetAlpha(0);
			if (currentPlayerSkills[i].skillImage != null) {
				skillButtons[i].GetComponent<Image>().sprite = currentPlayerSkills[i].skillImage;
			}
			skillButtons[i].onClick.RemoveAllListeners();
			int iCopy = i;
			skillButtons[i].onClick.AddListener(delegate { currentPlayerSkills[iCopy].TrySelectSkill(); } );
		}
	}

	public void SetSkillbuttonsVisiblity(bool isVisible) {
		if (isVisible) {
			Skills[] currentPlayerSkills = PlayerMoveController.instance.currentPlayer.skills;
			for (int i = 0; i < currentPlayerSkills.Length; ++i) {
				Debug.Log("SettingButtonVisible: " + i);
				skillButtons[i].gameObject.SetActive(true);
			}
			backButton.gameObject.SetActive(false);
			Skills.currentSkill = Skills.AllSkills.None;
		} else {
			foreach(Button skillButton in skillButtons) {
				skillButton.gameObject.SetActive(false);
			}
		}
	}
}
