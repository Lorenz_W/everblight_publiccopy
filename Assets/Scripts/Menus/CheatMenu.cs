﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatMenu : Menu {
	public void PlayerTurn() {
		TurnController.SetTurnTo(TurnController.TurnOrder.Player);
	}
	public void EnemyTurn() {
		TurnController.SetTurnTo(TurnController.TurnOrder.AI);
	}
	public void PauseTurn()
	{
		TurnController.SetTurnTo(TurnController.TurnOrder.Pause);
	}

	public void ToggleDebugOverlay()
	{
		IngameUI.isIngameUIShown = !IngameUI.isIngameUIShown;
	}

	//public void SetTurn(TurnController.TurnOrder turnToSet)
	//{
	//	TurnController.SetTurnTo(turnToSet);
	//}
}
