﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SystemMessageMenu : Menu {
	public static SystemMessageMenu instance = null;
	private void Awake() {
		if(instance == null) {
			instance = this;
		}
	}

	public void OpenMenu(string message) {
		base.OpenMenu();
		menuCanvas.gameObject.GetComponentInChildren<Text>().text = message;
		StartCoroutine(MessageAutoDisapear());
	}

	IEnumerator MessageAutoDisapear() {
		yield return new WaitForSeconds(5f);
		CloseMenu();
	}
}
