﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {
	[SerializeField] protected Canvas menuCanvas;
	protected bool isOpen = false;

	

	private void Start() {
		CloseMenu();
	}

	virtual protected void Update() {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (this.isOpen) {
				CloseMenu();
			}
		}
	}

	public void CloseMenu() {
		this.isOpen = false;
		this.menuCanvas.gameObject.SetActive(false);
	}
	virtual public void OpenMenu() {
		this.isOpen = true;
		this.menuCanvas.gameObject.SetActive(true);
	}

	public void ExitGame() {
		Application.Quit();
	}
}
