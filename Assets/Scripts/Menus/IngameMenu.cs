﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameMenu : Menu {
	public static void PauseGame() {
		TurnController.SetTurnTo(TurnController.TurnOrder.Pause);
	}

	protected override void Update() {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (!this.isOpen) {
				OpenMenu();
				SkillsMenu.instance.CloseMenu();
			} else {
				CloseMenu();
				SkillsMenu.instance.OpenMenu();
			}
		}
	}
}
