﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : Menu {
	[SerializeField] Slider sfxVolume;
	[SerializeField] Slider musicVolume;

	public void ChangeSFXVolume()
	{
		SoundManager.instance.sourceSFX.volume = sfxVolume.value;
	}
	public void ChangeMusicVolume()
	{
		SoundManager.instance.sourceMusic.volume = musicVolume.value;
	}
}
