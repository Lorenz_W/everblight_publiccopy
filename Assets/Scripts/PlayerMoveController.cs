﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveController : MonoBehaviour {
    [SerializeField] public Vector3 camDistanceToPlayers;
    [SerializeField] public Vector2Int[] startPositions;
    [SerializeField] public List<PlayerCharacter> playerCharacters = new List<PlayerCharacter>();
	[SerializeField] public PlayerCharacter currentPlayer = null;

	public static List<GridNode> playerPath = new List<GridNode>();
	public static GameObject currentHoveredObject = null;
	private static bool isInputLocked = false;
	public static bool IsInputLocked
	{
		get
		{
			return isInputLocked;
		}
		set
		{
			isInputLocked = value;
			if (value)
			{
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}
			else
			{
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
			}
		}
	}

	public static PlayerMoveController instance = null;
    private void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            Debug.LogWarning("More than one instance of PlayerMoveController in scene!");
        }
    }

    // Use this for initialization
    void Start() {
		if (currentPlayer == null) {
			Debug.LogWarning("Set currentPlayer in PlayerMoveController to a starting player!");
		}
		TurnController.SetTurnTo(TurnController.TurnOrder.Player);
        //sets the Icon and name of the currentPlayer
        GetComponent<ChangeIcon>().changeIcon(currentPlayer.name);
    }

    // Update is called once per frame
    void Update() {
		RaycastHit hitInfo;
		currentHoveredObject = Skills.ReturnHoveredObject(out hitInfo);
		//Debug.Log("ObjectCurrentlyHovered: " + currentHoveredObject.name);

		
		if (TurnController.GetTurnOf() == TurnController.TurnOrder.Player) {
			//InputLocked check so we skip this while LerpMovement is active.
			if (!IsInputLocked)
			{
				for (int i = 0; i < playerCharacters.Count; ++i)
				{
					if (playerCharacters[i].currentActionsLeft > 0)
					{
						break;
					}
					else if (i == playerCharacters.Count - 1)
					{
						TurnController.SetTurnTo(TurnController.TurnOrder.AI);
					}
				}
			}

			if (Input.GetMouseButtonDown(0) && !IsInputLocked) {
				if (currentHoveredObject != null && currentHoveredObject.tag == "Player" && Skills.currentSkill == Skills.AllSkills.None) {
					SwitchPlayer(currentHoveredObject);
				}
			}
			if (Input.GetKeyDown(KeyCode.Tab) && !IsInputLocked)
			{
				NextPlayer();
			}

			if (Skills.currentSkill == Skills.AllSkills.None && currentHoveredObject != null)
			{
				if (currentHoveredObject.tag == "Enemy")
				{
					GridSystem.CheckForBlockedNodes(false);
					GridSystem.visualizedSkillRange.Clear();
					GridSystem.visualizedSkillRange = GridSystem.GetNodesInRange(
						GridSystem.GetNodeFromWorldPosition(currentHoveredObject.transform.position),
						AIMoveController.instance.AIMoveRange,
						false);
				} else if (currentHoveredObject.tag == "Player")
				{
					GridSystem.CheckForBlockedNodes(true);
					GridSystem.visualizedSkillRange.Clear();
					GridSystem.visualizedSkillRange = GridSystem.GetNodesInRange(
						GridSystem.GetNodeFromWorldPosition(currentHoveredObject.transform.position),
						currentPlayer.skills[0].skillRange, //0 is always move, this is bad coding btw.
						currentHoveredObject.GetComponent<PlayerCharacter>().ignoresBlockedSpace);
				}
			}
		}
    }

	void SwitchPlayer(PlayerCharacter player)
	{
		currentPlayer = player;
		SkillsMenu.instance.OpenMenu();

		//Sets the Icon of the Player
		GetComponent<ChangeIcon>().changeIcon(player.gameObject.name);
		playerPath.Clear();
	}

	void SwitchPlayer(GameObject target) {
		//Camera.main.transform.parent = target.transform;
		//Camera.main.transform.localPosition = camDistanceToPlayers;
		SwitchPlayer(target.GetComponent<PlayerCharacter>());
	}

	void NextPlayer()
	{
		int current = playerCharacters.IndexOf(currentPlayer);
		if (current < 0)
		{
			Debug.LogWarning("Switching to next player failed!");
			return;
		}
		if (current == playerCharacters.Count -1)
		{
			SwitchPlayer(playerCharacters[0]);
		} else
		{
			SwitchPlayer(playerCharacters[current +1]);
		}
	}

	public void MovePlayerToTilePos(GameObject targetTile) {
		if (playerPath == null) {
			Debug.Log("No path set to move with!");
			++currentPlayer.currentActionsLeft;
			IsInputLocked = false;
			return;
		} else if (playerPath.Count < currentPlayer.skills[0].skillRange) {
			StartCoroutine(LerpMovement(
				currentPlayer.gameObject,
				playerPath.Count,
				playerPath));
		} else {
			StartCoroutine(LerpMovement(
				currentPlayer.gameObject,
				currentPlayer.skills[0].skillRange,
				playerPath));
		}
	}

    public void SetPlayerCharStartPos() {
        if(GridSystem.grid == null) {
            Debug.LogWarning("Grid not set.");
        }
        for (int i = 0; i < playerCharacters.Count; ++i) {
			playerCharacters[i].gameObject.transform.position = 
				GridSystem.grid[startPositions[i].x, startPositions[i].y].worldPosition 
				+ new Vector3(0f, playerCharacters[i].gameObject.transform.position.y, 0f);
        }
    }

	public IEnumerator LerpMovement(GameObject thingToMove, int steps, List<GridNode> path)
	{
		IsInputLocked = true;
		thingToMove.GetComponent<Animator>().Play("Walk");
		Debug.Log("Moving character " + thingToMove.name + " should move " + steps + " steps. Path size: " + path.Count);
		float speed;
		float rotationSpeed;
		if (thingToMove.GetComponent<PlayerCharacter>() != null)
		{
			speed = thingToMove.GetComponent<PlayerCharacter>().moveSpeed;
			rotationSpeed = thingToMove.GetComponent<PlayerCharacter>().rotationSpeed;
		}
		else if (thingToMove.GetComponent<EnemyCharacter>() != null)
		{
			speed = thingToMove.GetComponent<EnemyCharacter>().moveSpeed;
			rotationSpeed = thingToMove.GetComponent<EnemyCharacter>().rotationSpeed;
		}
		else
		{
			StopCoroutine(LerpMovement(thingToMove, steps, path));
			yield break;
		}
		Vector3 targetPos;
		Quaternion targetRotation = new Quaternion();
		path.Insert(0, GridSystem.GetNodeFromWorldPosition(currentPlayer.transform.position));
		for (int i = 1; i < steps+1; ++i)
		{
			Debug.Log("Moving character " + thingToMove.name + " step " + i);
			targetPos = path[i].worldPosition + new Vector3(0.0f, thingToMove.transform.position.y, 0.0f);
			targetRotation.eulerAngles = new Vector3(
				0,
				(path[i].gridX - path[i - 1].gridX) * 90,
				0);
			if(path[i].gridY - path[i - 1].gridY == -1)
			{
				targetRotation.eulerAngles = new Vector3(
				0,
				-180,
				0);
			}
			while (thingToMove.transform.rotation != targetRotation)
			{
				thingToMove.transform.rotation = Quaternion.RotateTowards(thingToMove.transform.rotation,
					targetRotation,
					rotationSpeed);
				yield return new WaitForSeconds(0.01f);
			}
			while (thingToMove.transform.position != targetPos)
			{
				thingToMove.transform.position = Vector3.MoveTowards(
						thingToMove.transform.position,
						targetPos,
						speed);
				yield return new WaitForSeconds(0.01f);
			}
		}
		thingToMove.GetComponent<Animator>().Play("Idle");
		GridSystem.UpdateFogOfWar();
		if(GridSystem.IsNodeLevelGoal(path[steps]))
		{
			//Reached the last level goal
			if(path[steps] == GridSystem.GetNodeFromWorldPosition(
				GridSystem.instance.levelGoal[GridSystem.instance.levelGoal.Length -1].position))
			{
				Debug.Log("Player win");
				TurnController.SetTurnTo(TurnController.TurnOrder.Win);
			}
			//Reached an intermediate level goal
			else
			{
				GridSystem.instance.ActivateNextLevelGoal();
				CutsceneTextController.instance.ActivateNextCutsceneText();
			}
		}

		playerPath.Clear();
		IsInputLocked = false;
	}
}
