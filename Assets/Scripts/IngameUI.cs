﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameUI : MonoBehaviour {
	public static bool isIngameUIShown = false;


	private void OnGUI()
	{
		if (isIngameUIShown)
		{
			GUILayout.Label(
				"Turn: " + TurnController.currentTurn + "\n"
				+ "CurrentPlayer: " + PlayerMoveController.instance.currentPlayer.name + "\n"
				+ "Actions left: " + PlayerMoveController.instance.currentPlayer.currentActionsLeft + "\n"
				+ "CurrentSkill: " + Skills.currentSkill + " \n"
				+ "PlayerCharacters: " + PlayerMoveController.instance.playerCharacters.Count + "\n"
				+ "Enemies: " + AIMoveController.instance.AICharacters.Length + "\n");
		}
	}
}
