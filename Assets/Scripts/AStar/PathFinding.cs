﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinding : MonoBehaviour {
	GridSystem grid;
    List<GridNode> path = new List<GridNode>();
	

	public static PathFinding instance = null;
	private void Awake() {
		if(instance == null) {
			instance = this;
		} else {
			Debug.Log("More than one instance of PathFinding in scene!");
			return;
		}
	}

	private void Start() {
		grid = GridSystem.instance;
	}

	public int GetDistance(GridNode nodeA, GridNode nodeB) {
		int distanceX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
		int distanceY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

		return distanceX + distanceY;
	}

	public List<GridNode> FindPath(Vector3 startPos, Vector3 targetPos, bool ignoreBlockedSpace) {
		GridNode startNode = GridSystem.GetNodeFromWorldPosition(startPos);
		GridNode targetNode = GridSystem.GetNodeFromWorldPosition(targetPos);
		//Debug.Log("Start node is: " + startNode.gridX + "," + startNode.gridY);
		//Debug.Log("Target node is: " + targetNode.gridX + "," + targetNode.gridY);

		List<GridNode> openNodes = new List<GridNode>();
		HashSet<GridNode> closedNodes = new HashSet<GridNode>();

		openNodes.Add(startNode);
		while(openNodes.Count > 0) {
			GridNode currentNode = openNodes[0];
			for (int i = 1; i < openNodes.Count; ++i) {
				if(openNodes[i].fCost < currentNode.fCost 
					|| openNodes[i].fCost == currentNode.fCost && openNodes[i].hCost < currentNode.hCost) {
					currentNode = openNodes[i];
				}
			}
			openNodes.Remove(currentNode);
			closedNodes.Add(currentNode);

			if(currentNode == targetNode) {
				path = RetracePath(startNode, targetNode);
				return path;
			}

			foreach(GridNode adjacentNode in GridSystem.GetAdjacentNodes(currentNode)) {
				if((adjacentNode.isBlocked && !ignoreBlockedSpace) || closedNodes.Contains(adjacentNode)) {
					continue;
				}

				int newMoveCostToAdjacent = currentNode.gCost + GetDistance(currentNode, adjacentNode);
				if(newMoveCostToAdjacent < adjacentNode.gCost || !openNodes.Contains(adjacentNode)) {
					adjacentNode.gCost = newMoveCostToAdjacent;
					adjacentNode.hCost = GetDistance(adjacentNode, targetNode);
					adjacentNode.parent = currentNode;

					if (!openNodes.Contains(adjacentNode)){
						openNodes.Add(adjacentNode);
					}
				}
			}	
		}
		//Debug.LogWarning("Path could not be found!");
		return null;
	}

	List<GridNode> RetracePath(GridNode startNode, GridNode endNode) {
		path = new List<GridNode>();
		GridNode currentNode = endNode;
		while(currentNode != startNode) {
			path.Add(currentNode);
			currentNode = currentNode.parent;
		}
		path.Reverse();

		//Debug.Log("Path has been asigned. " + path.ToString());

        return path;
	}
}