﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSystem : MonoBehaviour
{
	public float gridTileSize;
	/// <summary>
	/// Amount of grid tiles for each axis.
	/// </summary>
	public Vector2Int gridSize;
	[SerializeField] GameObject gridTilePrefab;
	[SerializeField] public Sprite emptyTile;
	[SerializeField] public Sprite playerRangeTile;
	[SerializeField] Sprite enemyRangeTile;
	[SerializeField] Sprite goalTile;
	[SerializeField] Sprite currentPlayeryTile;

	public LayerMask blockedSpaceMask;
	public static GridNode[,] grid;
	public static GameObject[,] gridTiles;
	public static List<GridNode> visualizedSkillRange = new List<GridNode>();

	public Transform[] levelGoal;
	public GameObject fogOfWarPrefab;

	public static List<GameObject> blockedSpaceObjects = new List<GameObject>();
	private static GameObject gridTilesParent;
	private static int currentGoalIndex = 0;
	public static GridSystem instance = null;
	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Debug.LogWarning("More than one instance of GridSystem in scene!");
		}
	}

	private void Start()
	{
		gridTilesParent = new GameObject("GridTiles");
		CreateGrid();
		PlayerMoveController.instance.SetPlayerCharStartPos();
		UpdateGrid(true);
		blockedSpaceObjects.Clear();
		foreach (GameObject gObj in FindObjectsOfType<GameObject>())
		{
			//Debug.Log("Found: " + gObj.name);
			if (blockedSpaceMask == (blockedSpaceMask | (1 << gObj.layer)))
			{
				blockedSpaceObjects.Add(gObj);
				Debug.Log("Added " + gObj.name + " to the blockedSpaceObjects list.");
			}
		}
	currentGoalIndex = 0;
	ActivateNextLevelGoal();
	}

	private void FixedUpdate()
	{
		if (Skills.IsTileMarkingActive)
		{
			if (Skills.MarkHoveredTile() != null)
			{
				Skills.targetObject = Skills.MarkHoveredTile();
			}
		}
		RedrawGridNodeSprites();
	}

	public static bool IsNodeOcupiedByAI(GridNode nodeToCheck)
	{
		foreach (EnemyCharacter AIChar in AIMoveController.instance.AICharacters)
		{
			if (nodeToCheck == GridSystem.GetNodeFromWorldPosition(AIChar.gameObject.transform.position))
			{
				return true;
			}
		}
		return false;
	}

	public static bool IsNodeOcupiedByPlayerChar(GridNode nodeToCheck)
	{
		foreach (PlayerCharacter playerCharacter in PlayerMoveController.instance.playerCharacters)
		{
			if (nodeToCheck == GridSystem.GetNodeFromWorldPosition(playerCharacter.transform.position))
			{
				return true;
			}
		}
		return false;
	}

	public static bool IsNodeLevelGoal(GridNode nodeToCheck)
	{
		if (instance.levelGoal == null || instance.levelGoal.Length <= 0)
		{
			return false;
		}
		foreach (Transform goalPart in GridSystem.instance.levelGoal)
		{
			if(!goalPart.gameObject.activeInHierarchy)
			{
				//Debug.Log("Intermediate goal is not active yet.");
				continue;
			}
			if (nodeToCheck == GridSystem.GetNodeFromWorldPosition(goalPart.position))
			{
				return true;
			}
		}
		return false;
	}

	/*
	private void OnDrawGizmos() {
		//Drawing a wirecube that encompasses the whole grid.
		Gizmos.DrawWireCube(
			new Vector3(
				(gridSize.x / 2) * gridTileSize,
				0.5f,
				(gridSize.y / 2) * gridTileSize),
			new Vector3(
				gridSize.x * gridTileSize,
				1f,
				gridSize.y * gridTileSize));

		//Drawing a gizmo plane(cube) for each node in 'grid'. 
		//Blocked nodes are collored red, the node where the current player is at is green.
		if (grid != null) {
			GridNode playerNode = GetNodeFromWorldPosition(PlayerMoveController.instance.currentPlayer.transform.position);
			foreach (GridNode node in grid) {
				if (node == playerNode) {
					//Debug.Log("PlayerPos is getting collored.");
					Gizmos.color = Color.green;
				}   else if (node.isBlocked) {
					continue;
					//Gizmos.color = Color.red;
				}   else if (PlayerMoveController.instance.playerPath != null && PlayerMoveController.instance.playerPath.Contains(node)) {
					if(PlayerMoveController.instance.playerPath.IndexOf(node) < PlayerMoveController.instance.currentPlayer.playerMoveRange) {
						Gizmos.color = Color.blue;
					} else {
						Gizmos.color = Color.black;
					}
				} else {
					Gizmos.color = Color.white;
				}
				Gizmos.DrawCube(node.worldPosition, new Vector3(gridTileSize - 0.1f, 0f, gridTileSize - 0.1f));
			}
		}
	}
	*/

	/// <summary>
	/// Instantiates the Grid by filling 'grid' with GridNodes according to grid- & tile size.
	/// </summary>
	public void CreateGrid()
	{
		grid = new GridNode[gridSize.x, gridSize.y];
		gridTiles = new GameObject[gridSize.x, gridSize.y];
		for (int i = 0; i < gridSize.x; ++i)
		{
			for (int j = 0; j < gridSize.y; ++j)
			{
				Vector3 tilePosition = new Vector3(
					(i * gridTileSize) + (gridTileSize / 2),
					0f,
					(j * gridTileSize) + (gridTileSize / 2));

				//Checks if there's an object colliding with a cube slightly smaller than tileSize, 
				//centered on the current tile.
				bool isAreaBlocked = Physics.CheckBox(
					tilePosition,
					new Vector3(gridTileSize / 2.1f, gridTileSize / 2.1f, gridTileSize / 2.1f),
					Quaternion.identity,
					blockedSpaceMask);

				grid[i, j] = new GridNode(isAreaBlocked, tilePosition)
				{
					//Debug.Log("Node Position: " + tilePosition + " is blocked: " + isAreaBlocked);
					gridX = i,
					gridY = j
				};

				gridTiles[i, j] = CreateTileObject(tilePosition, i, j);
				AddFogToNode(grid[i, j]);
			}
		}
	}

	private GameObject CreateTileObject(Vector3 tilePosition, int i, int j)
	{
		GameObject gridTile = Instantiate(gridTilePrefab);
		gridTile.name = "GridTile_" + i + "_" + j;
		gridTile.transform.position = tilePosition;
		gridTile.transform.parent = gridTilesParent.transform;
		return gridTile;
	}

	/// <summary>
	/// Returns a node from 'grid' which has the world position inside of its tile (ingoring y axis).
	/// </summary>
	/// <param name="worldPos"></param>
	/// <returns></returns>
	public static GridNode GetNodeFromWorldPosition(Vector3 worldPos)
	{
		//percentage placing on the grid of the position for the axis.
		float percentX = worldPos.x / (instance.gridSize.x * instance.gridTileSize);
		float percentY = worldPos.z / (instance.gridSize.y * instance.gridTileSize);

		//making sure we don't pass on dirty data.
		percentX = Mathf.Clamp01(percentX);
		percentY = Mathf.Clamp01(percentY);

		//
		int x = Mathf.RoundToInt((instance.gridSize.x - 1) * percentX);
		int y = Mathf.RoundToInt((instance.gridSize.y - 1) * percentY);
		return grid[x, y];
	}


	/// <summary>
	///  Returns a list of existing nodes that are horizontally or vertically adjacent to the node.
	/// </summary>
	/// <param name="node"></param>
	/// <returns></returns>
	public static List<GridNode> GetAdjacentNodes(GridNode node)
	{
		List<GridNode> adjacentNodes = new List<GridNode>();
		//Checking if the nodes that would be adjacent are actually existing in our grids size.
		if (node.gridX > 0)
		{
			adjacentNodes.Add(grid[node.gridX - 1, node.gridY]);
		}
		if (node.gridX < instance.gridSize.x - 1)
		{
			adjacentNodes.Add(grid[node.gridX + 1, node.gridY]);
		}
		if (node.gridY > 0)
		{
			adjacentNodes.Add(grid[node.gridX, node.gridY - 1]);
		}
		if (node.gridY < instance.gridSize.y - 1)
		{
			adjacentNodes.Add(grid[node.gridX, node.gridY + 1]);
		}
		//foreach (GridNode adjacentNode in adjacentNodes) {
		//Debug.Log("Adjacent node is: " + adjacentNode.gridX + "," + adjacentNode.gridY);
		//}
		return adjacentNodes;
	}

	public static void CheckForBlockedNodes(bool playersBlockNodes)
	{
		foreach (GridNode node in grid)
		{
			//Checks if there's an object colliding with a cube slightly smaller than tileSize, 
			//centered on the current tile.
			bool isAreaBlocked = Physics.CheckBox(
				node.worldPosition,
				new Vector3(0.1f, 2f, 0.1f),
				Quaternion.identity,
				instance.blockedSpaceMask);
			if (isAreaBlocked || IsNodeOcupiedByAI(node) || (IsNodeOcupiedByPlayerChar(node) && playersBlockNodes))
			{
				node.isBlocked = true;
			}
			else
			{
				node.isBlocked = false;
			}
		}
	}

	public static void UpdateGrid(bool playersBlockNodes)
	{
		CheckForBlockedNodes(playersBlockNodes);
		UpdateFogOfWar();
	}

	public static void RedrawGridNodeSprites()
	{
		if (grid == null)
		{
			Debug.LogWarning("Trying to Redraw grid node sprites without grid being initalized!");
			return;
		}

		foreach (GameObject tile in gridTiles)
		{
			tile.GetComponent<Animator>().enabled = false;
			tile.GetComponent<SpriteRenderer>().enabled = true;
			tile.GetComponent<SpriteRenderer>().sprite = instance.emptyTile;
			GridNode node = GetNodeFromWorldPosition(tile.transform.position);
			//Currently hovered tile
			if (PlayerMoveController.currentHoveredObject != null
				&& node == GetNodeFromWorldPosition(PlayerMoveController.currentHoveredObject.transform.position))
			{
				tile.GetComponent<Animator>().enabled = true;
				tile.GetComponent<Animator>().Play("PlayerGridHighlight");
			}
			//Selected player
			else if(node == GetNodeFromWorldPosition(PlayerMoveController.instance.currentPlayer.transform.position))
			{
				tile.GetComponent<SpriteRenderer>().sprite = instance.currentPlayeryTile;
			}
			//Level goals
			else if(IsNodeLevelGoal(node))
			{
				tile.GetComponent<SpriteRenderer>().sprite = instance.goalTile;
			}
			//Player path
			else if(PlayerMoveController.playerPath != null && PlayerMoveController.playerPath.Contains(node) && Skills.currentSkill == Skills.AllSkills.Move)
			{
				if(PlayerMoveController.playerPath.IndexOf(node) < PlayerMoveController.instance.currentPlayer.skills[0].skillRange)
				{
					tile.GetComponent<Animator>().enabled = true;
					tile.GetComponent<Animator>().Play("PlayerGridHighlight");
				}
				else
				{
					//tile.GetComponent<SpriteRenderer>().sprite = instance.playerRangeTile; too confusing without a different tile;
					tile.GetComponent<SpriteRenderer>().sprite = instance.emptyTile;
				}
			}
			//Skill/hovered move range
			else if (visualizedSkillRange.Contains(node))
			{
				gridTiles[node.gridX, node.gridY].GetComponent<SpriteRenderer>().sprite = instance.playerRangeTile;
			}
			//Blocked nodes
			else if (node.isBlocked && !IsNodeOcupiedByPlayerChar(node) && !IsNodeOcupiedByAI(node))
			{
				tile.GetComponent<SpriteRenderer>().enabled = false;
			}
		}
	}

	public static List<GridNode> GetNodesInRange(GridNode start, int range, bool addBlockedNodesToo)
	{
		List<GridNode> nodes = new List<GridNode>();
		nodes.Add(start);
		for (int i = 0; i < range; ++i)
		{
			//Debug.Log("Nodes in range round: " + i);

			int currentRunNodes = nodes.Count;
			for (int j = 0; j < currentRunNodes; ++j)
			{
				List<GridNode> adjacent = GetAdjacentNodes(nodes[j]);
				for (int k = 0; k < adjacent.Count; ++k)
				{
					if (nodes.Contains(adjacent[k]))
					{
						continue;
					}
					else if (adjacent[k].isBlocked && !addBlockedNodesToo)
					{
						continue;
					}
					//Debug.Log("Node added to range:" + adjacent[k].gridX + "_" + adjacent[k].gridY);
					nodes.Add(adjacent[k]);
				}
			}
		}
		return nodes;
	}

	public static void UpdateFogOfWar()
	{
		//foreach(GridNode node in grid)
		//{
		//	AddFogToNode(node);
		//}

		for (int i = 0; i < PlayerMoveController.instance.playerCharacters.Count; ++i)
		{
			foreach (GridNode node in GetNodesInRange(
				GridSystem.GetNodeFromWorldPosition(
					PlayerMoveController.instance.playerCharacters[i].transform.position),
				PlayerMoveController.instance.playerCharacters[i].fogClearRange,
				true))
			{
				RemoveFogFromNode(node);
			}
		}

		return;
	}

	private static void AddFogToNode(GridNode node)
	{
		if (node.fogOfWarObject != null)
		{
			return;
		}
		node.fogOfWarObject = Instantiate(instance.fogOfWarPrefab);
		node.fogOfWarObject.transform.position = node.worldPosition;
		node.fogOfWarObject.transform.parent = gridTiles[node.gridX, node.gridY].transform;
		node.hasFogOfWar = true;
		//Debug.Log("FogOfWar added to: " + node.gridX + "_" + node.gridY);
	}
	private static void RemoveFogFromNode(GridNode node)
	{
		Destroy(node.fogOfWarObject);
		node.hasFogOfWar = false;
		//Debug.Log("FogOfWar removed from: " + node.gridX + "_" + node.gridY);
	}

	public void ActivateNextLevelGoal()
	{
		if(currentGoalIndex >= levelGoal.Length)
		{
			Debug.LogWarning("Goal array/code handling it seems to be to missing something.");
			return;
		}
		foreach(Transform goal in levelGoal)
		{
			goal.gameObject.SetActive(false);
		}
		levelGoal[currentGoalIndex].gameObject.SetActive(true);
		++currentGoalIndex;
	}
}