﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridNode {
	/// <summary>
	/// Is true if an object occupies a part of this nodes space.
	/// </summary>
	public bool isBlocked;
	public Vector3 worldPosition;
	/// <summary>
	/// Distance to start node.
	/// </summary>
	public int gCost;
	/// <summary>
	/// Distance to target node.
	/// </summary>
	public int hCost;
	public int gridX;
	public int gridY;
	public GridNode parent;


	public bool hasTrap = false;
	public int trapImobilizeDuration;
	public GameObject trapObject;

	public bool hasBait;
	public GameObject baitObject;

	public bool hasFogOfWar;
	public GameObject fogOfWarObject;

	public int fCost {
		get {
			return gCost + hCost;
		}
	}

	public GridNode(bool isBlocked, Vector3 worldPos) {
		this.isBlocked = isBlocked;
		this.worldPosition = worldPos;
	}
}
