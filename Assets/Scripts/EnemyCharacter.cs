﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCharacter : MonoBehaviour {
	[SerializeField] public float moveSpeed;
	[SerializeField] public float rotationSpeed;
	public int roundsImobilized = -1;
}
