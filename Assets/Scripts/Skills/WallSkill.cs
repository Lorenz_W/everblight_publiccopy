﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallSkill : Skills {
	[SerializeField] GameObject wallPrefab;
	[SerializeField] int wallDuration;
	List<int> turnOfUse = new List<int>();
	List<GameObject> placedWalls = new List<GameObject>();

	override public bool TryUseSkill() {
		if (!base.TryUseSkill()) {
			return false;
		}
		if (targetNode.isBlocked) {
			Debug.Log("Node is blocked already, can't place a wall here.");
			return false;
		}

		turnOfUse.Add(TurnController.currentTurn);
		placedWalls.Add(Instantiate(wallPrefab));
		placedWalls[placedWalls.Count -1].transform.position = targetNode.worldPosition
				+ new Vector3(0f, placedWalls[placedWalls.Count -1].transform.position.y, 0f);
		placedWalls[placedWalls.Count -1].transform.parent = GridSystem.gridTiles[targetNode.gridX, targetNode.gridY].transform;
		GridSystem.blockedSpaceObjects.Add(placedWalls[placedWalls.Count -1]);
		Debug.Log("Wall placed at " + targetNode.gridX + "/" + targetNode.gridY);

		IsTileMarkingActive = false;
		GridSystem.UpdateGrid(true);
		return true;
	}

	override public bool TrySelectSkill() {
		if (!base.TrySelectSkill()) {
			return false;
		}
		IsTileMarkingActive = true;
		return true;
	}
	
	// Update is called once per frame
	void Update () {
		if (currentSkill == AllSkills.Wall) {
			if (targetObject != null) {
				if (Input.GetMouseButtonDown(0) && !PlayerMoveController.IsInputLocked) {
					TryUseSkill();
				}
			}
		}
		for (int i = 0; i < placedWalls.Count; ++i) {
			if (turnOfUse[i] + wallDuration <= TurnController.currentTurn) {
				Debug.Log("Wall destroyed due to duration end");
				Destroy(placedWalls[i]);
				GridSystem.blockedSpaceObjects.Remove(placedWalls[i]);
				placedWalls.Remove(placedWalls[i]);
			}
		}
	}
}
