﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Punch : Skills {
    override public bool TryUseSkill() {
        if (!base.TryUseSkill()) {
            return false;
        }

        Debug.Log("Target can be punched.");

        KnockbackEnemy(GridSystem.GetNodeFromWorldPosition(PlayerMoveController.instance.currentPlayer.transform.position), targetNode, targetObject);

		GridSystem.UpdateGrid(true);
		return true;
    }

    override public bool TrySelectSkill() {
        if (!base.TrySelectSkill()) {
            return false;
        }

		//GridSystem.visualizedSkillRange.Clear();
		//foreach (EnemyCharacter enemy in AIMoveController.instance.AICharacters)
		//{
		//	GridNode enemyNode = GridSystem.GetNodeFromWorldPosition(enemy.gameObject.transform.position);
		//	GridNode currentPlayerNode = GridSystem.GetNodeFromWorldPosition(
		//		PlayerMoveController.instance.currentPlayer.gameObject.transform.position);
		//	if (GridSystem.GetNodesInRange(currentPlayerNode, skillRange, true).Contains(enemyNode))
		//	{
		//		GridSystem.visualizedSkillRange.Add(enemyNode);
		//	}
		//}

		return true;
    }

    private void Update() {
        if (currentSkill == AllSkills.Punch) {
            targetObject = PlayerMoveController.currentHoveredObject;
            if (targetObject != null && targetObject.tag == "Enemy") {
                if (Input.GetMouseButtonDown(0) && !PlayerMoveController.IsInputLocked) {
                    TryUseSkill();
                }
            }
        }
    }

    private void KnockbackEnemy(GridNode player, GridNode punchedEnemy, GameObject enemy) {
        int xAxisDirection = 0;
        int yAxisDirection = 0;
        Debug.Log("Player " + player.gridX + "_" + player.gridY + " Enemy " + punchedEnemy.gridX + "_" + punchedEnemy.gridY);

        if(player.gridX > punchedEnemy.gridX) {
            xAxisDirection = -1;
        } else if (player.gridX < punchedEnemy.gridX) {
            xAxisDirection = 1;
        }
        if(player.gridY > punchedEnemy.gridY) {
            yAxisDirection = -1;
        } else if(player.gridY < punchedEnemy.gridY) {
            yAxisDirection = 1;
        }
		Debug.Log("xAxisDirection: " + xAxisDirection + " yAxisDirection: " + yAxisDirection);

		int previousTileX = punchedEnemy.gridX;
        int previousTileY = punchedEnemy.gridY;
		int i = punchedEnemy.gridX + xAxisDirection;
		int j = punchedEnemy.gridY + yAxisDirection;
		while (i < GridSystem.instance.gridSize.x && i >= 0 && j < GridSystem.instance.gridSize.y && j >= 0)
		{
			if (GridSystem.grid[i, j].isBlocked)
			{
				enemy.gameObject.transform.position =
					GridSystem.grid[previousTileX, previousTileY].worldPosition
					+ new Vector3(0f, enemy.gameObject.transform.position.y, 0f);

				Debug.Log("Punched enemy collided with blocking object at: " + i + "_" + j);
				return;
			}
			previousTileY = j;

			previousTileX = i;
			i = i + xAxisDirection;
			j = j + yAxisDirection;
		}

		Debug.Log("Punched enemy collided with end of grid at: " + previousTileX + "_" + previousTileY + "  Or something went wrong!");

		enemy.gameObject.transform.position =
            GridSystem.grid[previousTileX, previousTileY].worldPosition
            + new Vector3(0f, enemy.gameObject.transform.position.y, 0f);
        return;
    }
}
