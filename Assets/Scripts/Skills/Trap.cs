﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : Skills {
	[SerializeField] int imobilizationDuration;
	[SerializeField] GameObject trapPrefab;
	override public bool TryUseSkill() {
		if (!base.TryUseSkill()) {
			return false;
		}

		GameObject placedTrap = Instantiate(trapPrefab);
		placedTrap.transform.position = targetNode.worldPosition
				+ new Vector3(0f, placedTrap.transform.position.y, 0f);
		targetNode.hasTrap = true;
		targetNode.trapImobilizeDuration = imobilizationDuration;
		targetNode.trapObject = placedTrap;
		targetNode.trapObject.transform.parent = GridSystem.gridTiles[targetNode.gridX, targetNode.gridY].transform;
		Debug.Log("Trap placed at " + targetNode.gridX + "/" + targetNode.gridY);

		IsTileMarkingActive = false;
		GridSystem.UpdateGrid(true);
		return true;
	}

	override public bool TrySelectSkill() {
		if (!base.TrySelectSkill()) {
			return false;
		}
		IsTileMarkingActive = true;
		return true;
	}

	private void Update() {
		if (currentSkill == AllSkills.Trap) {
			if (targetObject != null) {
				if (Input.GetMouseButtonDown(0) && !PlayerMoveController.IsInputLocked) {
					TryUseSkill();
				}
			}
		}
	}
}
