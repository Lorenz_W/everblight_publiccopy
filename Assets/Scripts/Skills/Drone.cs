﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drone : Skills {
	[SerializeField] GameObject dronePrefab;

	override public bool TryUseSkill()
	{
		if (!base.TryUseSkill())
		{
			return false;
		}

		GameObject spawnedDrone = Instantiate(dronePrefab);
		spawnedDrone.transform.position = targetNode.worldPosition
				+ new Vector3(0f, spawnedDrone.transform.position.y, 0f);
		Debug.Log("Spawned drone at " + targetNode.gridX + "/" + targetNode.gridY);

		IsTileMarkingActive = false;
		GridSystem.UpdateGrid(true);
		return true;
	}

	override public bool TrySelectSkill()
	{
		if (!base.TrySelectSkill())
		{
			return false;
		}
		IsTileMarkingActive = true;
		return true;
	}

	private void Update()
	{
		if (currentSkill == AllSkills.Drone)
		{
			if (targetObject != null)
			{
				if (Input.GetMouseButtonDown(0) && !PlayerMoveController.IsInputLocked)
				{
					TryUseSkill();
				}
			}
		}
	}
}
