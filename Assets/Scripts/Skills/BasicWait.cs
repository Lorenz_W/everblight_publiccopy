﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicWait : Skills {
	override public bool TryUseSkill() {
		if (!base.TryUseSkill()) {
			return false;
		}
		PlayerMoveController.instance.currentPlayer.currentActionsLeft = 0;
		return true;
	}

	override public bool TrySelectSkill() {
		if (!base.TrySelectSkill()) {
			return false;
		}

		if (!TryUseSkill()) {
			return false;
		}

		return true;
	}
}
