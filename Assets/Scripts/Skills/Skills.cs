﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Skills : MonoBehaviour {
	[SerializeField] public AllSkills thisSkill;
	[SerializeField] public int skillUsesLeft;
	[SerializeField] public int skillRange = 0;
	[SerializeField] public Sprite skillImage = null;
	[SerializeField] public AudioClip skillUseSound = null;

	public static GameObject previousTarget = null;
	private static bool isTileMarkingActive = false;
	public static bool IsTileMarkingActive
	{
		get
		{
			return isTileMarkingActive;
		}
		set
		{
			isTileMarkingActive = value;
			if (value == true)
			{
				foreach (PlayerCharacter playerCharacter in PlayerMoveController.instance.playerCharacters)
				{
					playerCharacter.gameObject.GetComponent<Collider>().enabled = false;
				}
				foreach (EnemyCharacter enemyCharacter in AIMoveController.instance.AICharacters)
				{
					enemyCharacter.gameObject.GetComponent<Collider>().enabled = false;
				}
				foreach(GameObject blockingObject in GridSystem.blockedSpaceObjects)
				{
					blockingObject.gameObject.GetComponent<Collider>().enabled = false;
				}
				
			} else
			{
				foreach (PlayerCharacter playerCharacter in PlayerMoveController.instance.playerCharacters)
				{
					playerCharacter.gameObject.GetComponent<Collider>().enabled = true;
				}
				foreach (EnemyCharacter enemyCharacter in AIMoveController.instance.AICharacters)
				{
					enemyCharacter.gameObject.GetComponent<Collider>().enabled = true;
				}
				foreach (GameObject blockingObject in GridSystem.blockedSpaceObjects)
				{
					blockingObject.gameObject.GetComponent<Collider>().enabled = true;
				}
			}
		}
	}

	protected static GridNode targetNode;
	public static GameObject targetObject;

	public enum AllSkills {
		None,
		Wait,
		Move,
		Invisible,
		Wall,
		Trap,
		Bait,
		Punch,
        Drone
	}
	public static AllSkills currentSkill = AllSkills.None;


	
	virtual public bool TryUseSkill() {
		if (targetObject == null)
		{
			targetNode = GridSystem.GetNodeFromWorldPosition(PlayerMoveController.instance.currentPlayer.transform.position);
		} else
		{
			targetNode = GridSystem.GetNodeFromWorldPosition(targetObject.transform.position);
		}
		if (skillUsesLeft < 1) {
			Debug.Log("Can't use this skill anymore!");
			return false;
		}
		if(skillRange < PathFinding.instance.GetDistance(targetNode, GridSystem.GetNodeFromWorldPosition(PlayerMoveController.instance.currentPlayer.transform.position))){
			Debug.Log("Target out of skill use range!");
			return false;
		}

		if (skillUseSound != null)
		{
			SoundManager.instance.sourceSFX.PlayOneShot(skillUseSound);
		}
		--PlayerMoveController.instance.currentPlayer.currentActionsLeft;
		--skillUsesLeft;
		SkillsMenu.instance.OpenMenu();
		return true;
	}

	virtual public bool TrySelectSkill() {
		targetNode = null;
		targetObject = null;
		GridSystem.CheckForBlockedNodes(true);

		if (PlayerMoveController.instance.currentPlayer.currentActionsLeft < 1) {
			SystemMessageMenu.instance.OpenMenu(PlayerMoveController.instance.currentPlayer.gameObject.name + " has no actions left this turn!");
			return false;
		}

		currentSkill = this.thisSkill;

		GridSystem.visualizedSkillRange.Clear();
		GridSystem.visualizedSkillRange = GridSystem.GetNodesInRange(
			GridSystem.GetNodeFromWorldPosition(PlayerMoveController.instance.currentPlayer.transform.position),
			skillRange,
			false);

		return true;
	}


	public static GameObject MarkHoveredTile() {
		GameObject target = PlayerMoveController.currentHoveredObject;
		if(target == null) {
			return null;
		}

		if(target.tag == "GridTile") {
			return target;
		}
		return null;
	}

	public static GameObject ReturnHoveredObject(out RaycastHit hit) {
		GameObject target = null;
		if (PlayerMoveController.IsInputLocked)
		{
			hit = new RaycastHit();
			return null;
		}
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray.origin, ray.direction * 10, out hit)) {
			//Debug.Log("Object " + hit.collider.name + " is hovered.");
			target = hit.collider.gameObject;
		}
		return target;
	}
}
