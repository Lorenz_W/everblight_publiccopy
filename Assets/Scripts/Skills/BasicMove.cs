﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMove : Skills {
	override public bool TryUseSkill() {
		if (!base.TryUseSkill()) {
			PlayerMoveController.IsInputLocked = false;
			return false;
		}

		PlayerMoveController.instance.MovePlayerToTilePos(targetObject);
		Debug.Log("Player " + PlayerMoveController.instance.currentPlayer.gameObject.name + " used move skill.");

		IsTileMarkingActive = false;
		GridSystem.UpdateGrid(true);
		return true;
	}

	override public bool TrySelectSkill() {
		if (!base.TrySelectSkill()) {
			return false;
		}

		GridSystem.visualizedSkillRange.Clear();
		GridSystem.visualizedSkillRange = GridSystem.GetNodesInRange(
			GridSystem.GetNodeFromWorldPosition(PlayerMoveController.instance.currentPlayer.transform.position),
			skillRange,
			PlayerMoveController.instance.currentPlayer.ignoresBlockedSpace);

		IsTileMarkingActive = true;
		return true;
	}

	private void Update() {
		if (Skills.currentSkill == AllSkills.Move) {
			if (targetObject != null) {
				if (previousTarget != targetObject && targetObject.tag == "GridTile")
				{
					//Debug.Log("Searching for new move path.");
					PlayerMoveController.playerPath = PathFinding.instance.FindPath(
						PlayerMoveController.instance.currentPlayer.transform.position, 
						targetObject.transform.position,
						PlayerMoveController.instance.currentPlayer.ignoresBlockedSpace);
					previousTarget = targetObject;
				}

				if (Input.GetMouseButtonDown(0) && !PlayerMoveController.IsInputLocked) {
					PlayerMoveController.IsInputLocked = true;
					TryUseSkill();
				}
			}
		}
	}
}
