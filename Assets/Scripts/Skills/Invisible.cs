﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invisible : Skills {
	[SerializeField] int roundsInvisible = 2;
    [SerializeField] ParticleSystem[] invisibleVisualization;
    public ParticleSystem[] InvisibleVisualization {
        get {
			return invisibleVisualization;
        }
    }

	public static Invisible instance = null;
	private void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		else
		{
			Debug.LogWarning("More than one instance of Invisible (skill script) in scene!");
		}
	}

	override public bool TryUseSkill() {
		if (!base.TryUseSkill()) {
			Debug.Log("Couldn't execute invisibility skill.");
			return false;
		}
		foreach(ParticleSystem visualization in invisibleVisualization)
		{
			visualization.Play();
		}
		PlayerMoveController.instance.currentPlayer.roundsInvisible = roundsInvisible;
		Debug.Log("Set to invisible for " + roundsInvisible + " rounds.");
		return true;
	}
	override public bool TrySelectSkill() {
		if (!base.TrySelectSkill()) {
			return false;
		}

		TryUseSkill();

		return true;
	}
}
