﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bait : Skills {
	[SerializeField] GameObject baitPrefab;
	[SerializeField] int baitEffectRange;
	public static int placedBaitEffectRange;
	public static List<GridNode> baitLocactions = new List<GridNode>();

	override public bool TryUseSkill() {
		if (!base.TryUseSkill()) {
			return false;
		}

		placedBaitEffectRange = baitEffectRange;
		baitLocactions.Add(targetNode);

		GameObject placedBait = Instantiate(baitPrefab);
		placedBait.transform.position = targetNode.worldPosition
				+ new Vector3(0f, placedBait.transform.position.y, 0f);
		targetNode.baitObject = placedBait;
		targetNode.hasBait = true;
		targetNode.baitObject.transform.parent = GridSystem.gridTiles[targetNode.gridX, targetNode.gridY].transform;
		Debug.Log("Bait placed at " + targetNode.gridX + "/" + targetNode.gridY);

		IsTileMarkingActive = false;
		GridSystem.UpdateGrid(true);
		return true;
	}

	override public bool TrySelectSkill() {
		if (!base.TrySelectSkill()) {
			return false;
		}

		IsTileMarkingActive = true;
		return true;
	}

	private void Update() {
		if (currentSkill == AllSkills.Bait) {
			if (targetObject != null) {
				if (Input.GetMouseButtonDown(0) && !PlayerMoveController.IsInputLocked) {
					if (GridSystem.GetNodeFromWorldPosition(targetObject.transform.position).isBlocked)
					{
						Debug.Log("Can't use skill on a blocked space!");
						return;
					}
					TryUseSkill();
				}
			}
		}
	}

	
}
