﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour {
	[SerializeField] public int actionsPerTurn = 3;
	[SerializeField] public Skills[] skills;
	[SerializeField] public int fogClearRange;
	[SerializeField] public float moveSpeed = 10f;
	[SerializeField] public float rotationSpeed;
	[SerializeField] public bool ignoresBlockedSpace = false;
	public int roundsInvisible = -1;
	public int currentActionsLeft = -1;
	

	private void Start()
	{
		if (!PlayerMoveController.instance.playerCharacters.Contains(this))
		{
			PlayerMoveController.instance.playerCharacters.Add(this);
		}
		currentActionsLeft = actionsPerTurn;
	}
}
