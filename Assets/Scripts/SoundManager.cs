﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
	[SerializeField] public AudioSource sourceSFX;
	[SerializeField] public AudioSource sourceMusic;

	public static SoundManager instance;
	private void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		else
		{
			Debug.LogWarning("More than one instance of SoundManager in scene!");
		}
	}
}
