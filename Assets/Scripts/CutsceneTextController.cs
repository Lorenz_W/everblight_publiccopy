﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneTextController : MonoBehaviour {
	[SerializeField] CutsceneText[] cutsceneTexts;
	public int currentCutsceneTextIndex = -1;

	public static CutsceneTextController instance = null;
	private void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		else
		{
			Debug.LogWarning("More than one instance of CutsceneTextController in scene!");
		}
	}

	// Use this for initialization
	void Start () {
		StartShowingCutsceneText();
	}

	//// Update is called once per frame
	//void Update () {
	//	
	//}


	public void StartShowingCutsceneText()
	{
		Debug.Log("Controller started activating the first CutsceneText.");
		currentCutsceneTextIndex = 0;
		ActivateNextCutsceneText();
	}

	public void ActivateNextCutsceneText()
	{
		if(currentCutsceneTextIndex >= cutsceneTexts.Length)
		{
			Debug.Log("End of cutscene texts reached.");
			return;
		}

		if(currentCutsceneTextIndex > 0)
		{
			cutsceneTexts[currentCutsceneTextIndex -1].StopShowingText();
		}
		cutsceneTexts[currentCutsceneTextIndex].StartShowingText();

		++currentCutsceneTextIndex;
	}
}
