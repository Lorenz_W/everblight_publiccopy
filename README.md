﻿﻿﻿# EverBlight

## Description
This is the (history cropped) copy of a University Projects git repository.
EverBlight was one of our two semester projects for the 4th semester. 
It's a grid-based strategy game, balancing is still lacking/not there, narration & sound was basically ready but lacked time to fully implement.
Code commenting and cleanliness/structure are still lacking as well due to bad time managment/scoping after several sick days.

Copyright (c) 2019 by Lasse B. <lb95@outlook.de>, Max Bredlau <the@max-bredlau.de>, Lorenz W. <lorenz.weiland@ue-germany.de>
Max Bredlau: Art & Project Management
Lasse Buchholz: Game&Level Design & Sound
Lorenz Weiland: Code & Unity

## Required software
Unity 2018.2.7f1

## Getting Started
A\* gridsystem, if a\* is unfamiliar start with a quick tutorial on that. Otherwise with TurnController.

## License
All rights reserved.